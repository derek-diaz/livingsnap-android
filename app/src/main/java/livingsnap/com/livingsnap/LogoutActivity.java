package livingsnap.com.livingsnap;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;


public class LogoutActivity{

    public static final String URL_LOGOUT = "http://www.livingsnap.com/rest/v1/logout";

    static String token;
    static boolean result;

    public static void logoutUser(String user_token){
        token = user_token;
        invokeLogoutWS();
    }

    /**
     * Method that performs RESTful webservice invocations
     */
    private static void invokeLogoutWS(){

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("token", token);

        client.get(LogoutActivity.URL_LOGOUT, null ,new TextHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if(obj.getString("error").equals("false") && obj.getString("message").equals("success")){
                        // Navigate to initial activity screen
                        result = true;
                    }
                    //navigateToSplashScreenActivity();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable error) {

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);

                    result = false;

                    // When Http response code is '403'
                    if(statusCode == 403){
                        //Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '404'
                    else if(statusCode == 404){
                        //Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '500'
                    else if(statusCode == 500){
                        //Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code other than 404, 500
                    else{
                        //Toast.makeText(getApplicationContext(), "Unexpected Error occured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();

                    }

                    // Navigate to initial activity screen
                    //navigateToSplashScreenActivity();

                } catch (JSONException e) {
                    //Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    // Navigate to initial activity screen
                    //navigateToSplashScreenActivity();
                }
            }
        });
    }


}
