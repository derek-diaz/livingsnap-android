package livingsnap.com.livingsnap;

/**
 * Created by main on 2/23/15.
 */
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
    // Secure Shared Preferences
    SecurePreferences pref;

    // Context
    Context _context;

    // SecurePreferences (sharedprefs) file name
    private static final String PREF_NAME = "LivingSnapPref";

    private static final String TRUE = "true";
    private static final String FALSE = "false";

    // All Shared Preferences Keys
    private static final String IS_AUTHENTICATED = "IsAuth";

    //Private secure key - this needs to be improved
    private static final String _KEY = "laksdjf02ef@EWDF!#R#R$%";

    // User name (make variable public to access from outside)
    public static final String KEY_USERNAME = "username";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    // Token (make variable public to access from outside)
    public static final String TOKEN = "token";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = new SecurePreferences(_context, SessionManager.PREF_NAME, SessionManager._KEY, true);

    }

    /**
     * Create login session
     * */
    public void createLoginSession(String username, String token){

        // Storing login value as TRUE
        pref.put(SessionManager.IS_AUTHENTICATED, SessionManager.TRUE);

        // Storing username in pref
        pref.put(SessionManager.KEY_USERNAME, username);

        // Storing token in pref
        pref.put(SessionManager.TOKEN, token);


    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){

        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(SessionManager.KEY_USERNAME, pref.getString(SessionManager.KEY_USERNAME));

        // user email id
        user.put(SessionManager.KEY_EMAIL, pref.getString(SessionManager.KEY_EMAIL));

        // return user
        return user;
    }

    /**
     * Check login method will check user login status
     * If false it will redirect user to the initial page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!Boolean.parseBoolean(pref.getString(SessionManager.IS_AUTHENTICATED))){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, InitialActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Start Initial Activity
            _context.startActivity(i);
        }

    }

    /**
     * Clear session details
     * */
    public void logoutUser(){

        // Clearing all data from Shared Preferences
        pref.clear();

        // After logout redirect user to Initial Activity
        Intent i = new Intent(_context, InitialActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Starting Initial Activity
        _context.startActivity(i);
    }

    /**
     * Clear session details
     * */
    public void setUserEmail(String email){

        // Storing email in pref
        pref.put(SessionManager.KEY_EMAIL, email);
    }
}