package livingsnap.com.livingsnap;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;


public class RegisterActivity extends ActionBarActivity {

    public static final String URL_REGISTER = "http://www.livingsnap.com/rest/v1/users";

    SessionManager session;
    ProgressDialog prgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        session = new SessionManager(getApplicationContext());

        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        //Controller Declarations
        Button registerBtn = (Button) findViewById(R.id.btnRegister);
        final EditText txtUsername = (EditText) findViewById(R.id.txtUsername);
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        final EditText txtPassword = (EditText) findViewById(R.id.txtPassword);

        //Register Button Functionality
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, InitialActivity.class));

                // Get Email Edit View Value
                String username = txtUsername.getText().toString();
                // Get Email Edit View Value
                String email = txtEmail.getText().toString();
                // Get Password Edit View Value
                String password = txtPassword.getText().toString();

                // Instantiate Http Request Param Object
                RequestParams params = new RequestParams();
                // When Email Edit View and Password Edit View have values other than Null
                if(Utility.isNotNull(email) && Utility.isNotNull(password) && Utility.isNotNull(username)){
                    // When Email entered is Valid
                    if(Utility.validate(email)){
                        //Add email to session
                        session.setUserEmail(email);
                        // Put Http parameter username with value of Email Edit View control
                        params.put("email", email);
                        // Put Http parameter password with value of Password Edit Value control
                        params.put("password", password);
                        // Put Http parameter deviceID with value of device identifier
                        params.put("username", username);
                        // Invoke RESTful Web Service with Http parameters
                        invokeWS(params);

                        //Add params to session
                        session.pref.put("email", email);
                        session.pref.put("username", username);

                    }
                    // When Email is invalid
                    else{
                        Toast.makeText(getApplicationContext(), "Enter a valid email", Toast.LENGTH_LONG).show();
                    }
                } else{
                    Toast.makeText(getApplicationContext(), "Please enter all fields", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void invokeWS(RequestParams params){
        // Show Progress Dialog
        prgDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(RegisterActivity.URL_REGISTER, params ,new TextHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if(obj.getString("error").equals("false") && obj.getString("message").equals("success")){
                        //Toast.makeText(getApplicationContext(), "You are successfully logged in!", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Registration Successful! Please verify your e-mail address", Toast.LENGTH_LONG).show();

                        // Navigate to Login screen
                        navigateToInitialActivity();

                    }
                    // Else display error message
                    else{

                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }

            /**
             * Method which navigates from Register Activity to Initial Activity
             */
            public void navigateToInitialActivity(){
                Intent initIntent = new Intent(getApplicationContext(),InitialActivity.class);
                initIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(initIntent);
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable error) {

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);

                    // Hide Progress Dialog
                    prgDialog.hide();
                    // When Http response code is '403'
                    if(statusCode == 403){
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '404'
                    else if(statusCode == 404){
                        Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '500'
                    else if(statusCode == 500){
                        Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code other than 404, 500
                    else{
                        Toast.makeText(getApplicationContext(), "Unexpected Error occured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
