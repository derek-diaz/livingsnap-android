package livingsnap.com.livingsnap;

import org.apache.http.Header;
import org.json.JSONObject;
import org.json.JSONException;

import android.content.Intent;
import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;


public class LoginActivity extends ActionBarActivity {

    public static final String URL_LOGIN = "http://www.livingsnap.com/rest/v1/login";

    // Session Manager
    SessionManager session;
    // Progress Dialog Object
    ProgressDialog prgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button loginBtn = (Button) findViewById(R.id.btnLoginExecute);
        final EditText txtUsername = (EditText) findViewById(R.id.txtUsernameLogin);
        final EditText txtPassword = (EditText) findViewById(R.id.txtPasswordLogin);

        //Create session - note it is not authenticated at this point
        session = new SessionManager(getApplicationContext());

        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        //Login Button Functionality
        loginBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Get Email Edit View Value
                String email = txtUsername.getText().toString();
                // Get Password Edit View Value
                String password = txtPassword.getText().toString();

                session.setUserEmail(email);

                // Instantiate Http Request Param Object
                RequestParams params = new RequestParams();
                // When Email Edit View and Password Edit View have values other than Null
                if(Utility.isNotNull(email) && Utility.isNotNull(password)){
                    // When Email entered is Valid
                    if(Utility.validate(email)){
                        // Put Http parameter username with value of Email Edit View control
                        params.put("email", email);
                        // Put Http parameter password with value of Password Edit Value control
                        params.put("password", password);
                        // Put Http parameter deviceID with value of device identifier
                        params.put("deviceID", "Android5.5");
                        // Invoke RESTful Web Service with Http parameters
                        invokeWS(params);
                    }
                    // When Email is invalid
                    else{
                        Toast.makeText(getApplicationContext(), "Enter a valid email", Toast.LENGTH_LONG).show();
                    }
                } else{
                    Toast.makeText(getApplicationContext(), "Please enter all fields", Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void invokeWS(RequestParams params){
        // Show Progress Dialog
        prgDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(LoginActivity.URL_LOGIN, params ,new TextHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                // Hide Progress Dialog
                prgDialog.dismiss();
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    // When the JSON response has status boolean value assigned with true
                    if(obj.getString("error").equals("false") && obj.getString("message").equals("success")){
                        //Toast.makeText(getApplicationContext(), "You are successfully logged in!", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Success! Token: " + obj.getString("token"), Toast.LENGTH_LONG).show();



                        //TODO - Login service needs to return username for authenticated user
                        session.createLoginSession(session.pref.getString("username"), obj.getString("token"));


                        // Navigate to Main screen
                        navigateToMainActivity();
                    }
                    // Else display error message
                    else{

                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable error) {

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);

                    // Hide Progress Dialog
                    prgDialog.dismiss();
                    // When Http response code is '403'
                    if(statusCode == 403){
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '404'
                    else if(statusCode == 404){
                        Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '500'
                    else if(statusCode == 500){
                        Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code other than 404, 500
                    else{
                        Toast.makeText(getApplicationContext(), "Unexpected Error occured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    Log.e("ERROR",e.getMessage());

                }
            }
        });
    }

    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void navigateToMainActivity(){
        Intent mainIntent = new Intent(getApplicationContext(),MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
