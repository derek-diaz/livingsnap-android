package livingsnap.com.livingsnap;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity {

    public static final String URL_POST_IMAGE = "http://www.livingsnap.com/rest/v1/photos";
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static AsyncHttpClient client = new AsyncHttpClient();
    private Uri fileUri;
    //public static final int MEDIA_TYPE_IMAGE = 1;
    File image;
    ExifInterface exif;
    TextView mainMessage;
    TextView snapMessage;
    // Session Manager
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_main);

        //Instantiate session class
        session = new SessionManager(getApplicationContext());

        mainMessage = (TextView) findViewById(R.id.mainMessage);
        snapMessage = (TextView) findViewById(R.id.snapMessage);

        //Check to make sure user is authenticated or redirect them to initial activity
        session.checkLogin();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // email
        String email = user.get(SessionManager.KEY_EMAIL);

        // name
        String username = user.get(SessionManager.KEY_USERNAME);

        //If username wasn't set default to the email address w/out domain
        if (!Utility.isNotNull(username) && Utility.isNotNull(email)){
            username = email.substring(0,email.indexOf("@"));
        }


        // displaying user data
        mainMessage.setText("Go on " + username + "... ");

        //Initialize Camera button
        ImageButton cameraButton = (ImageButton) findViewById(R.id.snap);

        //Handle cameraButton functionality
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // create Intent to take a picture and return control to the calling application
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                File imagesFolder = new File(Environment.getExternalStorageDirectory(), "LivingSnap");
                imagesFolder.mkdirs();
                image = new File(imagesFolder, "LSIMG_" + timeStamp + ".jpg");
                fileUri = Uri.fromFile(image);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

                // start the image capture Intent
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

            }
        });

        // Checking camera availability
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device does't have camera
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                try {
                    //Bitmap photo = loadImageFromFile(image);
                    exif = new ExifInterface(image.getAbsolutePath());
                    //sendPhoto(photo);
                    sendPhoto();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Photo captured successfully
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
                Toast.makeText(getApplicationContext(),
                        "Image capture cancelled!", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // Image capture failed, advise user
                // user cancelled recording
                Toast.makeText(getApplicationContext(),
                        "Failed to capture image!", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_logout:
                doLogout();
                break;
            default:
                //do nothing
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void doLogout(){

        LogoutActivity.logoutUser(session.pref.getString("token"));
        session.pref.clear();
        navigateToSplashScreenActivity();
    }

    /**
     * Method which navigates from Register Activity to Initial Activity
     */
    public void navigateToSplashScreenActivity(){
        Intent splashIntent = new Intent(getApplicationContext(),SplashScreenActivity.class);
        splashIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(splashIntent);
    }

    /**
     * Checking device has camera hardware or not
     * */
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            return true;
        }
        else {
            return false;
        }
    }

    private void sendPhoto() throws Exception {

        RequestParams params = new RequestParams();
        params.put("photo", image);

        AsyncHttpResponseHandler responseHandler = new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(responseString);

                    // When Http response code is '403'
                    if(statusCode == 403){
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '404'
                    else if(statusCode == 404){
                        Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '500'
                    else if(statusCode == 500){
                        Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code other than 404, 500
                    else{
                        Toast.makeText(getApplicationContext(), "Unexpected Error occured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                    }

                    mainMessage.setText("Woops! Image upload failed...");
                    snapMessage.setText("Please try again.");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(responseString);
                    // When the JSON response has status boolean value assigned with true
                    if(obj.getString("error").equals("false") && obj.getString("message").equals("success")){
                        //Toast.makeText(getApplicationContext(), "You are successfully logged in!", Toast.LENGTH_LONG).show();
                        //Toast.makeText(getApplicationContext(), "File uploaded successfully!", Toast.LENGTH_LONG).show();
                        mainMessage.setText("Great! Image upload successful.");
                        snapMessage.setText("Let's do it again!!!");
                    }
                    // Else display error message
                    else{

                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }

            @Override
            public void onProgress(int bytesWritten, int totalSize){

                //Log.v(LOG_TAG, String.format("Progress %d from %d (%2.0f%%)", bytesWritten, totalSize, (totalSize > 0) ? (bytesWritten * 1.0 / totalSize) * 100 : -1));
                mainMessage.setText("Uploading your snap...");
                snapMessage.setText(String.format("%2.0f%%", (totalSize > 0) ? (bytesWritten * 1.0 / totalSize) * 100 : -1));
            }

            @Override
            public void onStart(){
                mainMessage.setText("Great snap!");
                snapMessage.setText("Beginning upload...");
            }
        };

        client.addHeader("token", session.pref.getString("token"));
        client.post(MainActivity.URL_POST_IMAGE, params , responseHandler);
    }

}
